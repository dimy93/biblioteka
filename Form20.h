#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace Biblioteka {

	/// <summary>
	/// Summary for Form2
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form2 : public System::Windows::Forms::Form
	{
	public:
		Form2( DataTable^ tabl, unsigned short it )
		{
			Table = tabl;
			Iterator = it;
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form2()
		{
			if (components)
			{
				delete components;
			}
		}
  private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
  protected: 
  private: System::Windows::Forms::TextBox^  textBoxMagazine;










  private: DataTable^ Table;
  private: unsigned short Iterator;
  private: System::Windows::Forms::Button^  buttonPrevious;
  private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel2;
  private: System::Windows::Forms::TextBox^  textBoxMagasine;


  private: System::Windows::Forms::Button^  buttonNext;
  private: System::Windows::Forms::PictureBox^  pictureBoxSnimka;
  private: System::Windows::Forms::TextBox^  textBoxKeyWords;
  private: System::Windows::Forms::Label^  label1;
  private: System::Windows::Forms::TextBox^  textBoxHeading;
  private: System::Windows::Forms::Label^  label2;
  private: System::Windows::Forms::Label^  label3;







	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
      this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
      this->textBoxMagasine = (gcnew System::Windows::Forms::TextBox());
      this->buttonNext = (gcnew System::Windows::Forms::Button());
      this->pictureBoxSnimka = (gcnew System::Windows::Forms::PictureBox());
      this->buttonPrevious = (gcnew System::Windows::Forms::Button());
      this->label3 = (gcnew System::Windows::Forms::Label());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->textBoxHeading = (gcnew System::Windows::Forms::TextBox());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->textBoxKeyWords = (gcnew System::Windows::Forms::TextBox());
      this->buttonNext = (gcnew System::Windows::Forms::Button());
      this->textBoxMagasine = (gcnew System::Windows::Forms::TextBox());
      this->tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
      this->pictureBoxSnimka = (gcnew System::Windows::Forms::PictureBox());
      this->tableLayoutPanel1->SuspendLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxSnimka))->BeginInit();
      this->tableLayoutPanel2->SuspendLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxSnimka))->BeginInit();
      this->SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this->tableLayoutPanel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
        | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->tableLayoutPanel1->ColumnCount = 4;
      this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        9)));
      this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        13.5F)));
      this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        22.5F)));
      this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        55)));
      this->tableLayoutPanel1->Controls->Add(this->textBoxMagazine, 1, 2);
      this->tableLayoutPanel1->Controls->Add(this->buttonNext, 2, 3);
      this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
      this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
      this->tableLayoutPanel1->RowCount = 4;
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel1->Size = System::Drawing::Size(200, 100);
      this->tableLayoutPanel1->TabIndex = 0;
      // 
      // textBoxMagazine
      // 
      this->tableLayoutPanel1->SetColumnSpan(this->textBoxMagazine, 2);
      this->textBoxMagazine->Location = System::Drawing::Point(21, 43);
      this->textBoxMagazine->Name = L"textBoxMagazine";
      this->textBoxMagazine->Size = System::Drawing::Size(66, 22);
      this->textBoxMagazine->TabIndex = 18;
      // 
      // buttonNext
      // 
      this->buttonNext->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->buttonNext->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
      this->buttonNext->Location = System::Drawing::Point(48, 63);
      this->buttonNext->Name = L"buttonNext";
      this->buttonNext->Size = System::Drawing::Size(39, 34);
      this->buttonNext->TabIndex = 17;
      this->buttonNext->Text = L"������� �����";
      this->buttonNext->UseVisualStyleBackColor = true;
      // 
      // pictureBoxSnimka
      // 
      this->pictureBoxSnimka->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
        | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->pictureBoxSnimka->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
      this->pictureBoxSnimka->Location = System::Drawing::Point(474, 3);
      this->pictureBoxSnimka->Name = L"pictureBoxSnimka";
      this->pictureBoxSnimka->Size = System::Drawing::Size(574, 557);
      this->pictureBoxSnimka->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
      this->pictureBoxSnimka->TabIndex = 15;
      this->pictureBoxSnimka->TabStop = false;
      // 
      // buttonPrevious
      // 
      this->buttonPrevious->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->buttonPrevious->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
      this->tableLayoutPanel2->SetColumnSpan(this->buttonPrevious, 2);
      this->buttonPrevious->Location = System::Drawing::Point(3, 553);
      this->buttonPrevious->Name = L"buttonPrevious";
      this->buttonPrevious->Size = System::Drawing::Size(221, 48);
      this->buttonPrevious->TabIndex = 3;
      this->buttonPrevious->Text = L"�������� �����";
      this->buttonPrevious->UseVisualStyleBackColor = true;
      // 
      // label3
      // 
      this->label3->AutoSize = true;
      this->label3->Location = System::Drawing::Point(3, 220);
      this->label3->Name = L"label3";
      this->label3->Size = System::Drawing::Size(80, 17);
      this->label3->TabIndex = 9;
      this->label3->Text = L"�������� :";
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(3, 110);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(70, 34);
      this->label2->TabIndex = 7;
      this->label2->Text = L"������� ���� :";
      // 
      // textBoxHeading
      // 
      this->textBoxHeading->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(3) {L"textBoxMagasine", L"textBox2", 
        L"textBox3"});
      this->tableLayoutPanel2->SetColumnSpan(this->textBoxHeading, 2);
      this->textBoxHeading->Location = System::Drawing::Point(94, 3);
      this->textBoxHeading->Name = L"textBoxHeading";
      this->textBoxHeading->Size = System::Drawing::Size(280, 22);
      this->textBoxHeading->TabIndex = 8;
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(3, 0);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(77, 17);
      this->label1->TabIndex = 5;
      this->label1->Text = L"�������� :";
      // 
      // textBoxKeyWords
      // 
      this->tableLayoutPanel2->SetColumnSpan(this->textBoxKeyWords, 2);
      this->textBoxKeyWords->Location = System::Drawing::Point(94, 113);
      this->textBoxKeyWords->Name = L"textBoxKeyWords";
      this->textBoxKeyWords->Size = System::Drawing::Size(283, 22);
      this->textBoxKeyWords->TabIndex = 6;
      // 
      // buttonNext
      // 
      this->buttonNext->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->buttonNext->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
      this->buttonNext->Location = System::Drawing::Point(230, 553);
      this->buttonNext->Name = L"buttonNext";
      this->buttonNext->Size = System::Drawing::Size(221, 48);
      this->buttonNext->TabIndex = 17;
      this->buttonNext->Text = L"������� �����";
      this->buttonNext->UseVisualStyleBackColor = true;
      // 
      // textBoxMagasine
      // 
      this->tableLayoutPanel2->SetColumnSpan(this->textBoxMagasine, 2);
      this->textBoxMagasine->Location = System::Drawing::Point(94, 223);
      this->textBoxMagasine->Name = L"textBoxMagasine";
      this->textBoxMagasine->Size = System::Drawing::Size(283, 22);
      this->textBoxMagasine->TabIndex = 18;
      // 
      // tableLayoutPanel2
      // 
      this->tableLayoutPanel2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
        | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->tableLayoutPanel2->ColumnCount = 4;
      this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        9)));
      this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        13.5F)));
      this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        22.5F)));
      this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        55)));
      this->tableLayoutPanel2->Controls->Add(this->textBoxMagasine, 1, 2);
      this->tableLayoutPanel2->Controls->Add(this->buttonNext, 2, 3);
      this->tableLayoutPanel2->Controls->Add(this->pictureBoxSnimka, 3, 0);
      this->tableLayoutPanel2->Controls->Add(this->textBoxKeyWords, 1, 1);
      this->tableLayoutPanel2->Controls->Add(this->label1, 0, 0);
      this->tableLayoutPanel2->Controls->Add(this->textBoxHeading, 1, 0);
      this->tableLayoutPanel2->Controls->Add(this->label2, 0, 1);
      this->tableLayoutPanel2->Controls->Add(this->label3, 0, 2);
      this->tableLayoutPanel2->Controls->Add(this->buttonPrevious, 0, 3);
      this->tableLayoutPanel2->Location = System::Drawing::Point(12, 12);
      this->tableLayoutPanel2->Name = L"tableLayoutPanel2";
      this->tableLayoutPanel2->RowCount = 4;
      this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 18.33183F)));
      this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 18.33183F)));
      this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 18.33183F)));
      this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 45.0045F)));
      this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel2->Size = System::Drawing::Size(1012, 604);
      this->tableLayoutPanel2->TabIndex = 17;
      // 
      // pictureBoxSnimka
      // 
      this->pictureBoxSnimka->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
        | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->pictureBoxSnimka->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
      this->pictureBoxSnimka->Location = System::Drawing::Point(457, 3);
      this->pictureBoxSnimka->Name = L"pictureBoxSnimka";
      this->tableLayoutPanel2->SetRowSpan(this->pictureBoxSnimka, 4);
      this->pictureBoxSnimka->Size = System::Drawing::Size(552, 598);
      this->pictureBoxSnimka->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
      this->pictureBoxSnimka->TabIndex = 15;
      this->pictureBoxSnimka->TabStop = false;
      // 
      // Form2
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(1058, 628);
      this->Controls->Add(this->tableLayoutPanel2);
      this->Name = L"Form2";
      this->Text = L"Form2";
      this->Load += gcnew System::EventHandler(this, &Form2::Form2_Load);
      this->tableLayoutPanel1->ResumeLayout(false);
      this->tableLayoutPanel1->PerformLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxSnimka))->EndInit();
      this->tableLayoutPanel2->ResumeLayout(false);
      this->tableLayoutPanel2->PerformLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxSnimka))->EndInit();
      this->ResumeLayout(false);

    }
#pragma endregion
  private: System::Void Form2_Load(System::Object^  sender, System::EventArgs^  e) {
    
    this -> textBoxHeading -> Text = this -> Table -> Rows[Iterator]["��������"] -> ToString();
    this -> textBoxMagazine -> Text = this -> Table -> Rows[Iterator]["��������"] -> ToString();
    this-> pictureBoxSnimka -> ImageLocation = this -> Table -> Rows[Iterator]["������"] -> ToString();
    //this->pictureBoxSnimka->DataBindings->Add(Picture);
           }
};
}
