
#pragma once
#include <windows.h>
#include <vcclr.h>
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace Biblioteka {

	/// <summary>
	/// Summary for Form2
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form2 : public System::Windows::Forms::Form
	{
	public:
		Form2( DataGridView^ tabl, unsigned long it )
		{
			Table = tabl;
			Iterator = it;
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form2()
		{
			if (components)
			{
				delete components;
			}
		}
	private: DataGridView^ Table;
  private: unsigned long Iterator;
  private: bool Data_Changed;
  private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
  protected: 
  private: System::Windows::Forms::TextBox^  textBoxMagasine;
  private: System::Windows::Forms::Button^  buttonNext;
  private: System::Windows::Forms::PictureBox^  pictureBoxSnimka;
  private: System::Windows::Forms::TextBox^  textBoxKeyWords;
  private: System::Windows::Forms::Label^  label1;
  private: System::Windows::Forms::TextBox^  textBoxHeading;
  private: System::Windows::Forms::Label^  label2;
  private: System::Windows::Forms::Label^  label3;
  private: System::Windows::Forms::Button^  buttonPrevious;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
      this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
      this->textBoxMagasine = (gcnew System::Windows::Forms::TextBox());
      this->buttonNext = (gcnew System::Windows::Forms::Button());
      this->pictureBoxSnimka = (gcnew System::Windows::Forms::PictureBox());
      this->textBoxKeyWords = (gcnew System::Windows::Forms::TextBox());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->textBoxHeading = (gcnew System::Windows::Forms::TextBox());
      this->label2 = (gcnew System::Windows::Forms::Label());
      this->label3 = (gcnew System::Windows::Forms::Label());
      this->buttonPrevious = (gcnew System::Windows::Forms::Button());
      this->tableLayoutPanel1->SuspendLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxSnimka))->BeginInit();
      this->SuspendLayout();
      // 
      // tableLayoutPanel1
      // 
      this->tableLayoutPanel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
        | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->tableLayoutPanel1->ColumnCount = 4;
      this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        9)));
      this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        13.5F)));
      this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        22.5F)));
      this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 
        55)));
      this->tableLayoutPanel1->Controls->Add(this->textBoxMagasine, 1, 2);
      this->tableLayoutPanel1->Controls->Add(this->buttonNext, 2, 3);
      this->tableLayoutPanel1->Controls->Add(this->pictureBoxSnimka, 3, 0);
      this->tableLayoutPanel1->Controls->Add(this->textBoxKeyWords, 1, 1);
      this->tableLayoutPanel1->Controls->Add(this->label1, 0, 0);
      this->tableLayoutPanel1->Controls->Add(this->textBoxHeading, 1, 0);
      this->tableLayoutPanel1->Controls->Add(this->label2, 0, 1);
      this->tableLayoutPanel1->Controls->Add(this->label3, 0, 2);
      this->tableLayoutPanel1->Controls->Add(this->buttonPrevious, 0, 3);
      this->tableLayoutPanel1->Location = System::Drawing::Point(12, 12);
      this->tableLayoutPanel1->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
      this->tableLayoutPanel1->RowCount = 4;
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 18.33183F)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 18.33183F)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 18.33183F)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 45.0045F)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
      this->tableLayoutPanel1->Size = System::Drawing::Size(997, 458);
      this->tableLayoutPanel1->TabIndex = 18;
      // 
      // textBoxMagasine
      // 
      this->tableLayoutPanel1->SetColumnSpan(this->textBoxMagasine, 2);
      this->textBoxMagasine->Location = System::Drawing::Point(92, 168);
      this->textBoxMagasine->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBoxMagasine->Name = L"textBoxMagasine";
      this->textBoxMagasine->Size = System::Drawing::Size(283, 22);
      this->textBoxMagasine->TabIndex = 18;
      this->textBoxMagasine->TextChanged += gcnew System::EventHandler(this, &Form2::textBoxMagasine_TextChanged);
      // 
      // buttonNext
      // 
      this->buttonNext->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->buttonNext->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
      this->buttonNext->Location = System::Drawing::Point(226, 408);
      this->buttonNext->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->buttonNext->Name = L"buttonNext";
      this->buttonNext->Size = System::Drawing::Size(218, 48);
      this->buttonNext->TabIndex = 17;
      this->buttonNext->Text = L"������� �����";
      this->buttonNext->UseVisualStyleBackColor = true;
      this->buttonNext->Click += gcnew System::EventHandler(this, &Form2::buttonNext_Click);
      // 
      // pictureBoxSnimka
      // 
      this->pictureBoxSnimka->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
        | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->pictureBoxSnimka->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
      this->pictureBoxSnimka->Location = System::Drawing::Point(450, 2);
      this->pictureBoxSnimka->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->pictureBoxSnimka->Name = L"pictureBoxSnimka";
      this->tableLayoutPanel1->SetRowSpan(this->pictureBoxSnimka, 4);
      this->pictureBoxSnimka->Size = System::Drawing::Size(544, 454);
      this->pictureBoxSnimka->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
      this->pictureBoxSnimka->TabIndex = 15;
      this->pictureBoxSnimka->TabStop = false;
      this->pictureBoxSnimka->DoubleClick += gcnew System::EventHandler(this, &Form2::pictureBoxSnimka_DoubleClick);
      this->pictureBoxSnimka->Click += gcnew System::EventHandler(this, &Form2::pictureBoxSnimka_Click);
      // 
      // textBoxKeyWords
      // 
      this->tableLayoutPanel1->SetColumnSpan(this->textBoxKeyWords, 2);
      this->textBoxKeyWords->Location = System::Drawing::Point(92, 85);
      this->textBoxKeyWords->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBoxKeyWords->Name = L"textBoxKeyWords";
      this->textBoxKeyWords->Size = System::Drawing::Size(283, 22);
      this->textBoxKeyWords->TabIndex = 6;
      this->textBoxKeyWords->TextChanged += gcnew System::EventHandler(this, &Form2::textBoxKeyWords_TextChanged);
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(3, 0);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(77, 17);
      this->label1->TabIndex = 5;
      this->label1->Text = L"�������� :";
      // 
      // textBoxHeading
      // 
      this->textBoxHeading->AutoCompleteCustomSource->AddRange(gcnew cli::array< System::String^  >(3) {L"textBoxMagasine", L"textBox2", 
        L"textBox3"});
      this->tableLayoutPanel1->SetColumnSpan(this->textBoxHeading, 2);
      this->textBoxHeading->Location = System::Drawing::Point(92, 2);
      this->textBoxHeading->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->textBoxHeading->Name = L"textBoxHeading";
      this->textBoxHeading->Size = System::Drawing::Size(280, 22);
      this->textBoxHeading->TabIndex = 8;
      this->textBoxHeading->TextChanged += gcnew System::EventHandler(this, &Form2::textBoxHeading_TextChanged);
      // 
      // label2
      // 
      this->label2->AutoSize = true;
      this->label2->Location = System::Drawing::Point(3, 83);
      this->label2->Name = L"label2";
      this->label2->Size = System::Drawing::Size(70, 34);
      this->label2->TabIndex = 7;
      this->label2->Text = L"������� ���� :";
      // 
      // label3
      // 
      this->label3->AutoSize = true;
      this->label3->Location = System::Drawing::Point(3, 166);
      this->label3->Name = L"label3";
      this->label3->Size = System::Drawing::Size(80, 17);
      this->label3->TabIndex = 9;
      this->label3->Text = L"�������� :";
      // 
      // buttonPrevious
      // 
      this->buttonPrevious->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
        | System::Windows::Forms::AnchorStyles::Right));
      this->buttonPrevious->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
      this->tableLayoutPanel1->SetColumnSpan(this->buttonPrevious, 2);
      this->buttonPrevious->Location = System::Drawing::Point(3, 408);
      this->buttonPrevious->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->buttonPrevious->Name = L"buttonPrevious";
      this->buttonPrevious->Size = System::Drawing::Size(217, 48);
      this->buttonPrevious->TabIndex = 3;
      this->buttonPrevious->Text = L"�������� �����";
      this->buttonPrevious->UseVisualStyleBackColor = true;
      this->buttonPrevious->Click += gcnew System::EventHandler(this, &Form2::buttonPrevious_Click);
      // 
      // Form2
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(1021, 494);
      this->Controls->Add(this->tableLayoutPanel1);
      this->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
      this->Name = L"Form2";
      this->Text = L"������";
      this->Load += gcnew System::EventHandler(this, &Form2::Form2_Load);
      this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Form2::Form2_FormClosed);
      this->tableLayoutPanel1->ResumeLayout(false);
      this->tableLayoutPanel1->PerformLayout();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxSnimka))->EndInit();
      this->ResumeLayout(false);

    }
#pragma endregion
  private: bool LoadInfo ( unsigned long it ) {
          if (( Table -> Rows -> Count )&&(it < unsigned long(Table -> Rows -> Count - 1) ))
          { 
            //String ^ orig = gcnew String(Table -> Rows[it]["Keyword"]->ToString()); //pin_ptr<const wchar_t> wch = PtrToStringChars(orig); wstring basicstring(wch); basicstring += _T(" (basic_string)");
            //std::istringstream temp (basicstring);
            //unsigned long i;
            //temp >> i;
            //int i = int :: Parse(orig); 
            this -> textBoxHeading -> Text = Table -> Rows[it]->Cells["��������"] ->Value->ToString();
            this -> textBoxMagasine -> Text = Table -> Rows[it]->Cells["��������"] ->Value->ToString();
            this-> pictureBoxSnimka -> ImageLocation = Table -> Rows[it]->Cells["������"] ->Value->ToString();
            Iterator = it;
            return true;
          }
          return false; 
         }
  private: bool ChangeInfo () {
          if ( Data_Changed )
           if ( System::Windows::Forms::MessageBox::Show("������� �� �� ��������� ���������?", "��������� �� ���������", System::Windows::Forms::MessageBoxButtons::YesNo)== System::Windows::Forms::DialogResult::Yes)
           {
             Table -> Rows[Iterator]->Cells["��������"] ->Value = this -> textBoxHeading -> Text;
             Table -> Rows[Iterator]->Cells["��������"] ->Value = this -> textBoxMagasine -> Text;
             Table -> Rows[Iterator]->Cells["������"] ->Value = this-> pictureBoxSnimka -> ImageLocation;
             return true; 
           }
          return false; 
         }
  
  private: System::Void buttonPrevious_Click(System::Object^  sender, System::EventArgs^  e) {
         ChangeInfo ();
         if ( this -> Iterator )
         {
             LoadInfo ( this -> Iterator - 1 );
             Data_Changed = false;
         }
       }
  private: System::Void Form2_Load(System::Object^  sender, System::EventArgs^  e) {
          LoadInfo ( this -> Iterator );
          Data_Changed = false;
         }
  private: System::Void buttonNext_Click(System::Object^  sender, System::EventArgs^  e) {
          ChangeInfo ();
          LoadInfo ( this -> Iterator + 1 );
          Data_Changed = false;
         }
private: System::Void textBoxHeading_TextChanged(System::Object^  sender, System::EventArgs^  e) {
          Data_Changed = true;
         }
private: System::Void textBoxKeyWords_TextChanged(System::Object^  sender, System::EventArgs^  e) {
          Data_Changed = true;
         }
private: System::Void textBoxMagasine_TextChanged(System::Object^  sender, System::EventArgs^  e) {
          Data_Changed = true;
         }
private: System::Void Form2_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
           ChangeInfo ();
         }
private: System::Void pictureBoxSnimka_Click(System::Object^  sender, System::EventArgs^  e) {

         }
private: System::Void pictureBoxSnimka_DoubleClick(System::Object^  sender, System::EventArgs^  e) {
    SHELLEXECUTEINFO shExecInfo;
    shExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    shExecInfo.fMask = NULL;
    shExecInfo.hwnd = NULL;
    shExecInfo.lpVerb = L"Open";
    pin_ptr<const wchar_t> str1 = PtrToStringChars(pictureBoxSnimka->ImageLocation->ToString());
    shExecInfo.lpFile = str1;
    shExecInfo.lpParameters = NULL;
    shExecInfo.lpDirectory = NULL;
    shExecInfo.nShow = SW_SHOWMAXIMIZED;
    shExecInfo.hInstApp = NULL;
    ShellExecuteEx(&shExecInfo);
         }
};
}
