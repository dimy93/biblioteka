#pragma once
namespace Biblioteka {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
		  CanResize = false;
			InitializeComponent();
      Table_Changed = false;
      this->oleDbDataAdapter1->Fill(this->dataSet11,"Table1"); 
      dataGridView1->DataSource = dataSet11->Tables["Table1"]->DefaultView;
      this->dataGridView1->Columns["ID"]->Visible = false;
      this->dataGridView1->Columns["Field1"]->Visible = false;
      this->dataGridView1->Columns["Keyword"]->Visible = false;
      unsigned long colc = dataGridView1->ColumnCount;
      unsigned long rowc = dataGridView1->RowCount;
      Col_Width = new double [colc];
      Row_Height = new double [rowc];
      dataGridView1->AutoResizeColumns();
      for (unsigned short i = 0; i < colc ; i++ )
      {
        Col_Width[i] = dataGridView1 -> Columns[i] -> Width;
      }
      for (unsigned short i = 0; i < rowc ; i++ )
      {
        Row_Height[i] = dataGridView1 -> Rows[i] -> Height;
      }
		}




  private: System::Windows::Forms::BindingSource^  dataSet1BindingSource;
  private: Biblioteka::DataSet1^  DataSet1;
  private: bool Table_Changed;
  private: double* Col_Width;
  private: double* Row_Height;
  private: bool CanResize;
  private: System::Windows::Forms::DataGridView^  dataGridView1;


  private: System::Windows::Forms::BindingSource^  table1BindingSource;



  public: 




  public: 

  public: 

  public: 
  protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
			delete [] Col_Width;
			delete [] Row_Height;
		}
  private: System::Data::OleDb::OleDbCommand^  oleDbSelectCommand1;
  protected: 
  private: System::Data::OleDb::OleDbConnection^  oleDbConnection1;
  private: System::Data::OleDb::OleDbCommand^  oleDbInsertCommand1;
  private: System::Data::OleDb::OleDbCommand^  oleDbUpdateCommand1;
  private: System::Data::OleDb::OleDbCommand^  oleDbDeleteCommand1;
  private: System::Data::OleDb::OleDbDataAdapter^  oleDbDataAdapter1;
  private: Biblioteka::DataSet1^  dataSet11;
  private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e);
  private: unsigned long Total_Column_Width ();
  private: unsigned long Total_Row_Height ();
  private: bool CResize();
  private: System::Void Form1_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
  private: System::Void dataGridView1_CellValueChanged(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e);
  private: System::Void dataGridView1_RowHeaderMouseDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
  private: System::Void Form1_Resize(System::Object^  sender, System::EventArgs^  e);
  private: System::Void dataGridView1_RowHeightChanged(System::Object^  sender, System::Windows::Forms::DataGridViewRowEventArgs^  e);
  private: System::Void dataGridView1_ColumnWidthChanged(System::Object^  sender, System::Windows::Forms::DataGridViewColumnEventArgs^  e);
  private: System::Void dataGridView1_RowsAdded(System::Object^  sender, System::Windows::Forms::DataGridViewRowsAddedEventArgs^  e);
  private: System::Void dataGridView1_RowsRemoved(System::Object^  sender, System::Windows::Forms::DataGridViewRowsRemovedEventArgs^  e);











  private: System::ComponentModel::IContainer^  components;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
      this->components = (gcnew System::ComponentModel::Container());
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
      this->oleDbSelectCommand1 = (gcnew System::Data::OleDb::OleDbCommand());
      this->oleDbConnection1 = (gcnew System::Data::OleDb::OleDbConnection());
      this->oleDbInsertCommand1 = (gcnew System::Data::OleDb::OleDbCommand());
      this->oleDbUpdateCommand1 = (gcnew System::Data::OleDb::OleDbCommand());
      this->oleDbDeleteCommand1 = (gcnew System::Data::OleDb::OleDbCommand());
      this->oleDbDataAdapter1 = (gcnew System::Data::OleDb::OleDbDataAdapter());
      this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
      this->table1BindingSource = (gcnew System::Windows::Forms::BindingSource(this->components));
      this->dataSet1BindingSource = (gcnew System::Windows::Forms::BindingSource(this->components));
      this->DataSet1 = (gcnew Biblioteka::DataSet1());
      this->dataSet11 = (gcnew Biblioteka::DataSet1());
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->BeginInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->table1BindingSource))->BeginInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataSet1BindingSource))->BeginInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->DataSet1))->BeginInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataSet11))->BeginInit();
      this->SuspendLayout();
      // 
      // oleDbSelectCommand1
      // 
      this->oleDbSelectCommand1->CommandText = L"SELECT * FROM Table1";
      this->oleDbSelectCommand1->Connection = this->oleDbConnection1;
      // 
      // oleDbConnection1
      // 
      this->oleDbConnection1->ConnectionString = L"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\".\\Mathematics.mdb\"";
      // 
      // oleDbInsertCommand1
      // 
      this->oleDbInsertCommand1->CommandText = L"INSERT INTO `Table1` (`��������`, `������� ���� 1`, `������� ���� 2`, `��������`," 
        L" `������`) VALUES (\?, \?, \?, \?, \?)";
      this->oleDbInsertCommand1->Connection = this->oleDbConnection1;
      this->oleDbInsertCommand1->Parameters->AddRange(gcnew cli::array< System::Data::OleDb::OleDbParameter^  >(5) {(gcnew System::Data::OleDb::OleDbParameter(L"��������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, L"��������")), (gcnew System::Data::OleDb::OleDbParameter(L"�������_����_1", 
        System::Data::OleDb::OleDbType::VarWChar, 0, L"������� ���� 1")), (gcnew System::Data::OleDb::OleDbParameter(L"�������_����_2", 
        System::Data::OleDb::OleDbType::VarWChar, 0, L"������� ���� 2")), (gcnew System::Data::OleDb::OleDbParameter(L"��������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, L"��������")), (gcnew System::Data::OleDb::OleDbParameter(L"������", System::Data::OleDb::OleDbType::VarWChar, 
        0, L"������"))});
      // 
      // oleDbUpdateCommand1
      // 
      this->oleDbUpdateCommand1->CommandText = resources->GetString(L"oleDbUpdateCommand1.CommandText");
      this->oleDbUpdateCommand1->Connection = this->oleDbConnection1;
      this->oleDbUpdateCommand1->Parameters->AddRange(gcnew cli::array< System::Data::OleDb::OleDbParameter^  >(16) {(gcnew System::Data::OleDb::OleDbParameter(L"��������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, L"��������")), (gcnew System::Data::OleDb::OleDbParameter(L"�������_����_1", 
        System::Data::OleDb::OleDbType::VarWChar, 0, L"������� ���� 1")), (gcnew System::Data::OleDb::OleDbParameter(L"�������_����_2", 
        System::Data::OleDb::OleDbType::VarWChar, 0, L"������� ���� 2")), (gcnew System::Data::OleDb::OleDbParameter(L"��������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, L"��������")), (gcnew System::Data::OleDb::OleDbParameter(L"������", System::Data::OleDb::OleDbType::VarWChar, 
        0, L"������")), (gcnew System::Data::OleDb::OleDbParameter(L"Original_ID", System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, 
        false, static_cast<System::Byte>(0), static_cast<System::Byte>(0), L"ID", System::Data::DataRowVersion::Original, nullptr)), 
        (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_��������", System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, 
        static_cast<System::Byte>(0), static_cast<System::Byte>(0), L"��������", System::Data::DataRowVersion::Original, true, nullptr)), 
        (gcnew System::Data::OleDb::OleDbParameter(L"Original_��������", System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, 
        false, static_cast<System::Byte>(0), static_cast<System::Byte>(0), L"��������", System::Data::DataRowVersion::Original, nullptr)), 
        (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_�������_����_1", System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, 
        static_cast<System::Byte>(0), static_cast<System::Byte>(0), L"������� ���� 1", System::Data::DataRowVersion::Original, true, 
        nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_�������_����_1", System::Data::OleDb::OleDbType::VarWChar, 
        0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), static_cast<System::Byte>(0), L"������� ���� 1", 
        System::Data::DataRowVersion::Original, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_�������_����_2", System::Data::OleDb::OleDbType::Integer, 
        0, System::Data::ParameterDirection::Input, static_cast<System::Byte>(0), static_cast<System::Byte>(0), L"������� ���� 2", 
        System::Data::DataRowVersion::Original, true, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_�������_����_2", 
        System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"������� ���� 2", System::Data::DataRowVersion::Original, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_��������", 
        System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, static_cast<System::Byte>(0), static_cast<System::Byte>(0), 
        L"��������", System::Data::DataRowVersion::Original, true, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_��������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"��������", System::Data::DataRowVersion::Original, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_������", 
        System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, static_cast<System::Byte>(0), static_cast<System::Byte>(0), 
        L"������", System::Data::DataRowVersion::Original, true, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"������", System::Data::DataRowVersion::Original, nullptr))});
      // 
      // oleDbDeleteCommand1
      // 
      this->oleDbDeleteCommand1->CommandText = resources->GetString(L"oleDbDeleteCommand1.CommandText");
      this->oleDbDeleteCommand1->Connection = this->oleDbConnection1;
      this->oleDbDeleteCommand1->Parameters->AddRange(gcnew cli::array< System::Data::OleDb::OleDbParameter^  >(11) {(gcnew System::Data::OleDb::OleDbParameter(L"Original_ID", 
        System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"ID", System::Data::DataRowVersion::Original, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_��������", 
        System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, static_cast<System::Byte>(0), static_cast<System::Byte>(0), 
        L"��������", System::Data::DataRowVersion::Original, true, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_��������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"��������", System::Data::DataRowVersion::Original, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_�������_����_1", 
        System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, static_cast<System::Byte>(0), static_cast<System::Byte>(0), 
        L"������� ���� 1", System::Data::DataRowVersion::Original, true, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_�������_����_1", 
        System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"������� ���� 1", System::Data::DataRowVersion::Original, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_�������_����_2", 
        System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, static_cast<System::Byte>(0), static_cast<System::Byte>(0), 
        L"������� ���� 2", System::Data::DataRowVersion::Original, true, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_�������_����_2", 
        System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"������� ���� 2", System::Data::DataRowVersion::Original, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_��������", 
        System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, static_cast<System::Byte>(0), static_cast<System::Byte>(0), 
        L"��������", System::Data::DataRowVersion::Original, true, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_��������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"��������", System::Data::DataRowVersion::Original, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"IsNull_������", 
        System::Data::OleDb::OleDbType::Integer, 0, System::Data::ParameterDirection::Input, static_cast<System::Byte>(0), static_cast<System::Byte>(0), 
        L"������", System::Data::DataRowVersion::Original, true, nullptr)), (gcnew System::Data::OleDb::OleDbParameter(L"Original_������", 
        System::Data::OleDb::OleDbType::VarWChar, 0, System::Data::ParameterDirection::Input, false, static_cast<System::Byte>(0), 
        static_cast<System::Byte>(0), L"������", System::Data::DataRowVersion::Original, nullptr))});
      // 
      // oleDbDataAdapter1
      // 
      this->oleDbDataAdapter1->DeleteCommand = this->oleDbDeleteCommand1;
      this->oleDbDataAdapter1->InsertCommand = this->oleDbInsertCommand1;
      this->oleDbDataAdapter1->SelectCommand = this->oleDbSelectCommand1;
      cli::array< System::Data::Common::DataColumnMapping^ >^ __mcTemp__1 = gcnew cli::array< System::Data::Common::DataColumnMapping^  >(6) {(gcnew System::Data::Common::DataColumnMapping(L"ID", 
        L"ID")), (gcnew System::Data::Common::DataColumnMapping(L"��������", L"��������")), (gcnew System::Data::Common::DataColumnMapping(L"������� ���� 1", 
        L"������� ���� 1")), (gcnew System::Data::Common::DataColumnMapping(L"������� ���� 2", L"������� ���� 2")), (gcnew System::Data::Common::DataColumnMapping(L"��������", 
        L"��������")), (gcnew System::Data::Common::DataColumnMapping(L"������", L"������"))};
      this->oleDbDataAdapter1->TableMappings->AddRange(gcnew cli::array< System::Data::Common::DataTableMapping^  >(1) {(gcnew System::Data::Common::DataTableMapping(L"Table", 
        L"Table1", __mcTemp__1))});
      this->oleDbDataAdapter1->UpdateCommand = this->oleDbUpdateCommand1;
      // 
      // dataGridView1
      // 
      this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
      this->dataGridView1->Location = System::Drawing::Point(0, 0);
      this->dataGridView1->Name = L"dataGridView1";
      this->dataGridView1->RowTemplate->Height = 24;
      this->dataGridView1->Size = System::Drawing::Size(1044, 524);
      this->dataGridView1->TabIndex = 17;
      this->dataGridView1->CellValueChanged += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &Form1::dataGridView1_CellValueChanged);
      this->dataGridView1->RowHeightChanged += gcnew System::Windows::Forms::DataGridViewRowEventHandler(this, &Form1::dataGridView1_RowHeightChanged);
      this->dataGridView1->RowsAdded += gcnew System::Windows::Forms::DataGridViewRowsAddedEventHandler(this, &Form1::dataGridView1_RowsAdded);
      this->dataGridView1->RowHeaderMouseDoubleClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &Form1::dataGridView1_RowHeaderMouseDoubleClick);
      this->dataGridView1->RowsRemoved += gcnew System::Windows::Forms::DataGridViewRowsRemovedEventHandler(this, &Form1::dataGridView1_RowsRemoved);
      this->dataGridView1->ColumnWidthChanged += gcnew System::Windows::Forms::DataGridViewColumnEventHandler(this, &Form1::dataGridView1_ColumnWidthChanged);
      // 
      // table1BindingSource
      // 
      this->table1BindingSource->DataMember = L"Table1";
      this->table1BindingSource->DataSource = this->dataSet1BindingSource;
      // 
      // dataSet1BindingSource
      // 
      this->dataSet1BindingSource->DataSource = this->DataSet1;
      this->dataSet1BindingSource->Position = 0;
      // 
      // DataSet1
      // 
      this->DataSet1->DataSetName = L"DataSet1";
      this->DataSet1->SchemaSerializationMode = System::Data::SchemaSerializationMode::IncludeSchema;
      // 
      // dataSet11
      // 
      this->dataSet11->DataSetName = L"DataSet1";
      this->dataSet11->SchemaSerializationMode = System::Data::SchemaSerializationMode::IncludeSchema;
      // 
      // Form1
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->AutoScroll = true;
      this->ClientSize = System::Drawing::Size(1044, 524);
      this->Controls->Add(this->dataGridView1);
      this->Name = L"Form1";
      this->Text = L"����������";
      this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
      this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Form1::Form1_FormClosing);
      this->Resize += gcnew System::EventHandler(this, &Form1::Form1_Resize);
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->EndInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->table1BindingSource))->EndInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataSet1BindingSource))->EndInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->DataSet1))->EndInit();
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataSet11))->EndInit();
      this->ResumeLayout(false);

    }
#pragma endregion
  
};
}