#include "stdafx.h"
#include "Form1.h"
#include "Form2.h"
#include <cmath>
using namespace Biblioteka;
bool Form1::CResize()
{
  unsigned long colc = dataGridView1->ColumnCount, rowc = dataGridView1->RowCount;
  if ( colc )
  {
    unsigned long tcw = Total_Column_Width(), formw =  dataGridView1 -> Size.Width ;
    CanResize = false;
    for( unsigned short i = 0; i < colc; i++ )
    {
      Col_Width[i] *= double (formw) / double(tcw);
      dataGridView1->Columns[i]->Width = int( floor( Col_Width[i] ) );
    }
    tcw = Total_Column_Width();
    dataGridView1->Columns[colc-1]->Width += formw - tcw - 2;
    CanResize = true;
  }
  if ( rowc )
  {
    unsigned long trh = Total_Row_Height(), formh =  dataGridView1 -> Size.Height ;
    CanResize = false;
    for (unsigned short i = 0; i < rowc ;i++ )
    {
      Row_Height[i] *= double (formh) / double(trh);
      dataGridView1->Rows[i] -> Height = int( floor( Row_Height[i] ) );
    }
    trh = Total_Row_Height();
    dataGridView1 -> Rows[rowc-1] -> Height += formh - trh-2;
    CanResize = true;
  }
  return( colc && rowc );
}
System::Void Form1::Form1_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) 
{
  if ( Table_Changed )
    if ( System::Windows::Forms::MessageBox::Show("������� �� �� �������� ���������?", "��������� �� ���������", System::Windows::Forms::MessageBoxButtons::YesNo)== System::Windows::Forms::DialogResult::Yes)
      this->oleDbDataAdapter1->Update(dataSet11);
}
System::Void Form1::dataGridView1_CellValueChanged(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
{
  Table_Changed = true;
}
System::Void Form1::dataGridView1_RowHeaderMouseDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e)
{
  if ( e->RowIndex < dataSet11 -> Tables["Table1"] -> Rows -> Count )
  {
    Form2^ secondForm = gcnew Form2(dataGridView1,e->RowIndex );
    secondForm->Show();
  }
}
System::Void Form1::Form1_Resize(System::Object^  sender, System::EventArgs^  e)
{
  if (CanResize)
    CResize();
}
System::Void Form1::dataGridView1_RowsRemoved(System::Object^  sender, System::Windows::Forms::DataGridViewRowsRemovedEventArgs^  e) 
{
  if (CanResize)
  {
    delete [] Row_Height;
    Row_Height = new double [dataGridView1->RowCount];
    for (unsigned short i = 0; i < dataGridView1->RowCount ; i++ )
    {
      Row_Height[i] = dataGridView1 -> Rows[i] -> Height;
    } 
    CResize();
    Table_Changed = true;
  } 
}
System::Void Form1::dataGridView1_RowHeightChanged(System::Object^  sender, System::Windows::Forms::DataGridViewRowEventArgs^  e)
{
  if ( CanResize )
  {
    CanResize = false;
    int temp = dataGridView1 -> Rows[e->Row->Index] -> Height - int ( floor( Row_Height[e->Row->Index] ) );
    Row_Height[e->Row->Index] += temp;
    this->Height += temp;
    CanResize = true;
  }
}
System::Void Form1::dataGridView1_ColumnWidthChanged(System::Object^  sender, System::Windows::Forms::DataGridViewColumnEventArgs^  e)
{
  if ( CanResize )
  {
    CanResize = false;
    int temp = dataGridView1 -> Columns[e->Column->Index] -> Width - int ( floor( Col_Width[e->Column->Index] ) );
    Col_Width[e->Column->Index] += temp;
    Width += temp;
    CanResize = true;
  }
}
System::Void Form1::dataGridView1_RowsAdded(System::Object^  sender, System::Windows::Forms::DataGridViewRowsAddedEventArgs^  e) 
{
 if (CanResize)
 {
   delete [] Row_Height;
   Row_Height = new double [dataGridView1->RowCount];
   for (unsigned short i = 0; i < dataGridView1->RowCount ; i++ )
   {
     Row_Height[i] = dataGridView1 -> Rows[i] -> Height;
   } 
   CResize();
 }
}
unsigned long Form1::Total_Row_Height ()
{
  unsigned long Return = dataGridView1->ColumnHeadersHeight;
  unsigned long rowc = dataGridView1->RowCount;
  for (unsigned short i = 0; i < rowc ;i++ )
  {
    if ( dataGridView1->Rows[i]->Visible )
      Return += unsigned long ( floor(Row_Height[i]) );
  }
  return Return;
}
unsigned long Form1::Total_Column_Width ()
{
  unsigned long Return = dataGridView1->RowHeadersWidth;
  unsigned long colc = dataGridView1->ColumnCount;
  for (unsigned short i = 0; i < colc ;i++ )
  {
    if ( dataGridView1->Columns[i]->Visible )
      Return += unsigned long( floor( Col_Width[i] ) );
  }
  return Return;
}
System::Void Form1::Form1_Load(System::Object^  sender, System::EventArgs^  e) 
{
  CanResize = true;
  CResize(); 
}
Form1::Form1(void)
{
  CanResize = false;
	InitializeComponent();
  Table_Changed = false;
  this->oleDbDataAdapter1->Fill(this->dataSet11,"Table1"); 
  dataGridView1->DataSource = dataSet11->Tables["Table1"]->DefaultView;
  this->dataGridView1->Columns["ID"]->Visible = false;
  this->dataGridView1->Columns["Field1"]->Visible = false;
  this->dataGridView1->Columns["Keyword"]->Visible = false;
  //this->dataGridView1->Columns["��������"]->DisplayIndex = dataGridView->Columns->GetFirstColumn( DataGridViewElementStates::Visible );
  unsigned long colc = dataGridView1->ColumnCount;
  unsigned long rowc = dataGridView1->RowCount;
  Col_Width = new double [colc];
  Row_Height = new double [rowc];
  dataGridView1->AutoResizeColumns();
  for (unsigned short i = 0; i < colc ; i++ )
  {
    Col_Width[i] = dataGridView1 -> Columns[i] -> Width;
  }
  for (unsigned short i = 0; i < rowc ; i++ )
  {
    Row_Height[i] = dataGridView1 -> Rows[i] -> Height;
  }
}

//NO SAVE ON THE NEW COLOMNS-NO IDEA WHY????